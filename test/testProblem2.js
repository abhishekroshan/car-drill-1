const lastCarInInventory = require("../problems/problem2");
const inventory = require("../inventory");

const result = lastCarInInventory(inventory);

if(result){
    console.log(`Last car is a ${result.car_make} ${result.car_model}`);
}else{
    console.log("Inventory is Empty");
}

