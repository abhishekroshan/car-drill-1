const inventory = require('../inventory');       //importing inventory
const findCarById = require("../problems/problem1");

const id = 33;
const result = findCarById(inventory,id);

if(result){
    console.log(`Car ${id} is a ${result.car_year} ${result.car_make} ${result.car_model}`);
}else {
    console.log(`No car found with the id ${id}`);
}

// const result = findCar(inventory,33);  //storing the result

// console.log(`Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`);