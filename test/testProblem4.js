const manufacturingYears = require("../problems/problem4");
const inventory = require("../inventory");

const result = manufacturingYears(inventory);

if(result.length>0){
    console.log(result)
}else{
    console.log('Inventory is Empty')
}
