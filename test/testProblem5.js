const carYears = require("../problems/problem4");
const inventory = require("../inventory");
const oldCarsThan2000 = require("../problems/problem5");

const allCars = carYears(inventory);

const result = oldCarsThan2000(allCars);

if(allCars.length>0){
    console.log(result);
}else{
    console.log("No cars found");
}

