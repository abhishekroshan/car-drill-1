function lastCarInInventory(inventory){

    if (Array.isArray(inventory===false)) {
        console.error('Inventory is not an Array');
        return null;
      }

    return inventory[inventory.length-1];  //using the .length funtion to get to the last index of inventory
}

module.exports = lastCarInInventory;
