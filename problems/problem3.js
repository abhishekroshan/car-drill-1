function carSorting(inventory){

    if (!Array.isArray(inventory)) {
        console.error('Inventory is not an Array');
        return [];
      }


    const newInventory = [];      //create new array to store names

    for(let index=0;index<inventory.length;index++){
        newInventory.push(inventory[index].car_model);  //pushing car names to array
    }

    newInventory.sort();  //sorting array

    return newInventory;
}

module.exports = carSorting;