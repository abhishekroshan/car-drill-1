function manufacturingYears(inventory){

    if (!Array.isArray(inventory)) {
        console.error("Inventory is not an Array");
        return [];
      }

    const years= [];      //create new array to store years

    for(let index=0;index<inventory.length;index++){
        years.push(inventory[index].car_year);  //pushing car years to array
    }

    return years;
}

module.exports = manufacturingYears;