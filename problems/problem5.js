function oldCarsThan2000(inventory){

    if (!Array.isArray(inventory)) {
        console.error("Inventory is not an Array");
        return [];
      }
    
    let count = 0;

    for(let index=0;index<inventory.length;index++){
        if(inventory[index]<2000){
            count++;
        }
    }
    return count;
}

module.exports = oldCarsThan2000;