function bmwAndAudiCars(inventory) {

  if (!Array.isArray(inventory)) {
    console.error("Inventory is not an Array");
    return [];
  }

    const bmwAndAudi = [];

    for (let index = 0; index < inventory.length; index++) {
      if (inventory[index].car_make === 'BMW' || inventory[index].car_make === 'Audi') {
        bmwAndAudi.push(inventory[index]);
      }
    }

    return bmwAndAudi;
  }
  
module.exports = bmwAndAudiCars;
  